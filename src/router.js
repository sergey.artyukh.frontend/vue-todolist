import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

import todoList from './components/todoList.vue';
import activePage from './components/activePage.vue';
import completedPage from './components/completedPage.vue';

export default new Router ({
    mode: "history",
    routes: [
       { path: '/', component: todoList },
       { path: '/active', component: activePage },
       { path: '/completed', component: completedPage },
    ],
});
