import Vue from 'vue';

import todoList from './components/todoList.vue';
import loginPage from './components/loginPage.vue';

import headerTodo from './components/headerTodo.vue';
import mainTodo from './components/mainTodo.vue';
import activePage from './components/activePage.vue';

import loader from './components/loader.vue';
Vue.component('loader', loader);



Vue.component('loginPage', loginPage);
Vue.component('todoList', todoList);

Vue.component('activePage', activePage);
Vue.component('headerTodo', headerTodo);
Vue.component('mainTodo', mainTodo);
